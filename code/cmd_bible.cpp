#include "json.c"

/* TODO:
    1. [X] Search for passages where a word/phrase appears
        - [ ] Include an option to find X verses of context
    2. [X] Bounds checking on input
    3. [ ] Commentaries
        - [ ] Not actually sure commentaries exist in json format
    4. [ ] Decide how to handle +1 / -1 offsets
*/

#define ArraySize(Array) (sizeof((Array))/sizeof((Array)[0]))

char * BibleBooks[] = {
    "Genesis", 
    "Exodus", 
    "Leviticus", 
    "Numbers", 
    "Deuteronomy", 
    "Joshua", 
    "Judges", 
    "Ruth", 
    "1 Samuel", 
    "2 Samuel", 
    "1 Kings", 
    "2 Kings", 
    "1 Chronicles", 
    "2 Chronicles", 
    "Ezra", 
    "Nehemiah", 
    "Tobit", 
    "Judith", 
    "Esther", 
    "1 Maccabees", 
    "2 Maccabees", 
    "Job", 
    "Psalms", 
    "Proverbs", 
    "Ecclesiastes", 
    "Song of Songs", 
    "Wisdom", 
    "Sirach", 
    "Isaiah", 
    "Jeremiah", 
    "Lamentations", 
    "Baruch", 
    "Ezekiel", 
    "Daniel", 
    "Hosea", 
    "Joel", 
    "Amos", 
    "Obadiah", 
    "Jonah", 
    "Micah", 
    "Nahum", 
    "Habakkuk", 
    "Zephaniah", 
    "Haggai", 
    "Zechariah", 
    "Malachi", 
    "Matthew", 
    "Mark", 
    "Luke", 
    "John", 
    "Acts of the Apostles", 
    "Romans", 
    "1 Corinthians", 
    "2 Corinthians", 
    "Galatians", 
    "Ephesians", 
    "Philippians", 
    "Colossians", 
    "1 Thessalonians", 
    "2 Thessalonians", 
    "1 Timothy", 
    "2 Timothy", 
    "Titus", 
    "Philemon", 
    "Hebrews", 
    "James", 
    "1 Peter", 
    "2 Peter", 
    "1 John", 
    "2 John", 
    "3 John", 
    "Jude", 
    "Revelation"
};

static void PrintAllBooks(bool Numbered = false)
{
    for(int Index = 0; Index < ArraySize(BibleBooks); Index++)
    {
	if(Numbered)
	{
	    printf("%d. ", Index + 1);
	}
	printf("%s\n", BibleBooks[Index]);
    }
}

static char * ReadEntireFileAndNullTerminate(char * Filename, uint32_t * FileSize)
{
    char * Result = 0;

    FILE * File = fopen(Filename, "r");
    if(File)
    {
	fseek(File, 0L, SEEK_END);
	*FileSize = 0;
	*FileSize += ftell(File);
	rewind(File);

	Result = (char*)malloc(sizeof(char)*(*FileSize));
	fread(Result, 1, *FileSize, File);
	fclose(File);

	Result[(*FileSize)] = 0;
    }
    else
    {
	printf("Could not find file %s", Filename);
    }

    return Result;
}

static bool StringMatch(char * A, char * B);
static bool StringContains(char * A, char * B);

static int BookNameToInt(char * Book)
{
    int Result = 0;

    for(int BookNum = 0; BookNum < 73; BookNum++)
    {
	if(StringMatch(BibleBooks[BookNum], Book))
	{
	    Result = BookNum;
	    return(Result);
	}
    }
    Result = -1;
    return(Result);
}

static int FuzzyFindBookName(char * Book)
{
    int Result = BookNameToInt(Book);
    if(Result == -1)
    {
	for(int BookNum = 0; BookNum < 73; BookNum++)
	{
	    if(StringContains(BibleBooks[BookNum], Book))
	    {
		Result = BookNum;
		return Result;
	    }
	}
    }
    return(Result);
}

uint32_t Pow(uint32_t Base, uint32_t Exponent)
{
    if(Exponent == 0)
    {
	return(1);
    }
    else
    {
	return(Base * Pow(Base, Exponent - 1));
    }
}

static uint32_t StringLength(char * A)
{
    uint32_t Result = 0;

    while(*A)
    {
	Result++;
	A++;
    }

    return Result;
}

// NOTE: Checks if strings are equal to each other by comparing each char
static bool StringCheck(char * A, char * B)
{
    bool Result = false;
    while(*A && *B)
    {
	if(*A != *B)
	{
	    return false;
	}
	else
	{
	    A++;
	    B++;
	}
    }
    if((*A && !(*B)) || (*B && !(*A)))
    {
	return Result;
    }
    else
    {
	Result = true;
	return Result;
    }
}

static uint32_t HashString(char * A)
{
    uint32_t Result = 0;
    int CurrentPower = StringLength(A) - 1;
    while((*A))
    {
	Result = Result + (((*A) * Pow(10, CurrentPower)) % UINT32_MAX);
	A++;
	CurrentPower--;
    }

    return Result;
}

static uint32_t HashLength(char * A, uint32_t Length)
{
    uint32_t Result = 0;
    int CurrentPower = Length - 1;
    while((*A) && CurrentPower + 1)
    {
	Result = Result + (((*A)*Pow(10, CurrentPower)) % UINT32_MAX);
	A++;
	CurrentPower--;
    }
    return(Result);
}

static bool StringMatch(char * A, char * B)
{
    if(HashString(A) == HashString(B))
    {
	while((*A) && (*B))
	{
	    if(*A == *B)
	    {
		A++;
		B++;
	    }
	    else
	    {
		return(false);
	    }
	}
	if((*A && !(*B)) || (*B && !(*A)))
	{
	    return(false);
	}
	return(true);
    }
    return(false);
}

static uint32_t RabinKarp(char * A, char * B)
{
    uint32_t PatternHash = HashString(B);
    uint32_t PatternSize = StringLength(B);
    uint32_t CurrentHash = HashLength(A, PatternSize);

    uint32_t Pos = 0;
    while(*A)
    {
	if(PatternHash == CurrentHash)
	{
	    StringCheck(A, B);
	    return Pos;
	}
	else
	{
	    CurrentHash -= ((*A)*Pow(10, PatternSize - 1));
	    CurrentHash *= 10;
	    CurrentHash = (CurrentHash + *(A + PatternSize)) % UINT32_MAX;
	}
	Pos++;
	A++;
    }
    return UINT32_MAX;
}

static bool StringContains(char * A, char * B)
{
    if(RabinKarp(A, B) != UINT32_MAX)
    {
	return true;
    }
    else
    {
	return false;
    }
}

static bool StringContainsChar(char * A, char B)
{
    while(*A)
    {
	if(*A == B)
	{
	    return true;
	}
	A++;
    }
    return false;
}

static bool StringContainsNumber(char * A)
{
    for(uint8_t Number = (uint8_t)'0'; Number != (uint8_t)'9'; Number++)
    {
	bool Result = StringContainsChar(A, Number);
	if(Result)
	{
	    return Result;
	}
    }
    return false;
}

static void print_depth_shift(int depth)
{
        int j;
        for (j=0; j < depth; j++) {
                printf(" ");
        }
}

static void process_value(json_value* value, int depth);

static void process_object(json_value* value, int depth)
{
        int length, x;
        if (value == NULL) {
                return;
        }
        length = value->u.object.length;
        for (x = 0; x < length; x++) {
                print_depth_shift(depth);
                printf("object[%d].name = %s\n", x, value->u.object.values[x].name);
                process_value(value->u.object.values[x].value, depth+1);
        }
}

static void process_array(json_value* value, int depth)
{
        int length, x;
        if (value == NULL) {
                return;
        }
        length = value->u.array.length;
        printf("array\n");
        for (x = 0; x < length; x++) {
                process_value(value->u.array.values[x], depth);
        }
}

static void Split(char * Source, char ** Target)
{
    char * Start = Source;
    while(*Source != ' ')
    {
	Source++;
    }
    (*Target) = (char*)malloc(Source - Start + 2); 

    int Counter = 0;
    while(Start != Source)
    {
	(*Target)[Counter] = *Start;
	Counter++;
	Start++;
    }
    (*Target)[Counter] = 0;
}

static void process_value(json_value* value, int depth)
{
        if (value == NULL) {
                return;
        }
        if (value->type != json_object) {
                print_depth_shift(depth);
        }
        switch (value->type) {
                case json_none:
                        printf("none\n");
                        break;
                case json_null:
                        printf("null\n");
                        break;
                case json_object:
                        process_object(value, depth+1);
                        break;
                case json_array:
                        process_array(value, depth+1);
                        break;
                case json_integer:
                        printf("int: %10ld\n", (long)value->u.integer);
                        break;
                case json_double:
                        printf("double: %f\n", value->u.dbl);
                        break;
                case json_string:
                        printf("string: %s\n", value->u.string.ptr);
                        break;
                case json_boolean:
                        printf("bool: %d\n", value->u.boolean);
                        break;
        }
}

static json_value * GetChild(json_value * Parent)
{
    return Parent->u.object.values[1].value->u.object.values[0].value;
}

static char * GetPassage(json_value * BibleText, int Book, int Chapter, int Verse,
			 bool Corrected = true)
{
    // NOTE: Adjust the numbers for 0 indexing and charset being the first member of the json
    if(!Corrected)
    {
	Book++;
	Chapter--;
	Verse--;
    }

    char * Result = 0;

    // TODO: Bounds checking!!! 
    BibleText = BibleText->u.object.values[Book].value->u.object.values[Chapter].value;
    Result = BibleText->u.object.values[Verse].value->u.string.ptr;

    return Result;
}

static char ** GetMultiplePassages(json_value * BibleText, int Book,
				  int Chapter, int Verse, int EndVerse)
{
    char ** Passages; 
    int NumPassages = EndVerse - Verse + 1;
    // TODO: Bounds checking!!! 
    Passages = (char**)malloc(sizeof(char*)*NumPassages);
    for(int Index = 0; Index < NumPassages; Index++)
    {
	Passages[Index] = GetPassage(BibleText, Book, Chapter, Verse + Index);
    }
    return Passages;
}

static int StringToInt(char * String)
{
    int Result = 0;
    int Length = StringLength(String);
    int CurrentPower = Length - 1;

    while(*String)
    {
	Result += (atoi(String))*Pow(10, CurrentPower);
	CurrentPower--;
	String++;
    }
    return Result;
}

static int StringLengthToInt(char * String, char * End)
{
    int Result = 0;
    int Length = End - String;
    int CurrentPower = Length - 1;

    while(String != (End + 1))
    {
	Result += (atoi(String))*Pow(10, CurrentPower);
	CurrentPower--;
	String++;
    }
    return Result;
}

struct BiblePassage
{
    int Book, Chapter, Verse;
};

static void SearchForPhrase(json_value * BibleJson, char * Phrase)
{
    int NumBooks = BibleJson->u.object.length;
    for(int BookIndex = 1; BookIndex < NumBooks; BookIndex++)
    {
	json_value * BookJson = BibleJson->u.object.values[BookIndex].value;

	int NumChapters = BibleJson->u.object.values[BookIndex].value->u.object.length;
	for(int ChapterIndex = 0; ChapterIndex < NumChapters; ChapterIndex++)
	{

	    json_value * ChapterJson = BookJson->u.object.values[ChapterIndex].value;
	    int NumVerses = ChapterJson->u.object.length;
	    for(int VerseIndex = 0; VerseIndex < NumVerses; VerseIndex++)
	    {
		json_char * CurrentVerse =
		    ChapterJson->u.object.values[VerseIndex].value->u.string.ptr;
		if(RabinKarp(CurrentVerse, Phrase) != UINT32_MAX)
		{
		    printf("%s ", BibleJson->u.object.values[BookIndex].name);
		    printf("Chapter %s:%s %s\n", BookJson->u.object.values[ChapterIndex].name,
			ChapterJson->u.object.values[VerseIndex].name, CurrentVerse);
		}
	    }
	}
    }
}

static void PrintEntireBible(json_value * BibleJson)
{
    int NumBooks = BibleJson->u.object.length;
    for(int BookIndex = 1; BookIndex < NumBooks; BookIndex++)
    {
	printf("%s\n", BibleJson->u.object.values[BookIndex].name);
	json_value * BookJson = BibleJson->u.object.values[BookIndex].value;

	int NumChapters = BibleJson->u.object.values[BookIndex].value->u.object.length;
	for(int ChapterIndex = 0; ChapterIndex < NumChapters; ChapterIndex++)
	{
	    printf("\tChapter %s: \n",
		   BookJson->u.object.values[ChapterIndex].name);

	    json_value * ChapterJson = BookJson->u.object.values[ChapterIndex].value;
	    int NumVerses = ChapterJson->u.object.length;
	    for(int VerseIndex = 0; VerseIndex < NumVerses; VerseIndex++)
	    {
		printf("\t%s %s\n", ChapterJson->u.object.values[VerseIndex].name,
		       ChapterJson->u.object.values[VerseIndex].value->u.string.ptr);
	    }
	}
    }
}

static char * GetPtrToChar(char * String, char Character)
{
    while(String && *String != Character)
    {
	String++;
    }
    if(String)
    {
	return String;
    }
    else
    {
	return 0;
    }
}

static int GetPosOfCharInString(char * String, char Character)
{
    int Result = 0;
    while(*String)
    {
	if(*String == Character)
	{
	    return Result;
	}
	Result++;
	String++;
    }
    Result = -1;
    return Result;
}

static bool IsValidBookNumber(json_value *BibleJson, uint32_t BookNumber)
{
    // NOTE: Subtracting 1 for the charset info object
    return(BookNumber >= 1 &&
	   BookNumber <= BibleJson->u.object.length - 1);
}

static json_value * GetBook(json_value * BibleJson, uint32_t BookNumber)
{
    json_value * Result = 0;
    if(IsValidBookNumber(BibleJson, BookNumber))
    {
	Result = BibleJson->u.object.values[BookNumber].value;
    }
    
    return Result;
}

static bool IsValidChapterNumber(json_value *BibleJson,
				 uint32_t BookNumber, uint32_t ChapterNumber)
{
    if(IsValidBookNumber(BibleJson, BookNumber))
    {
	return(ChapterNumber + 1 >= 1 &&
	       ChapterNumber + 1 <= GetBook(BibleJson, BookNumber)->u.object.length);
    }
    return false;
}

static json_value * GetChapter(json_value * BibleJson, uint32_t BookNumber, uint32_t ChapterNumber)
{
    if(IsValidChapterNumber(BibleJson, BookNumber, ChapterNumber))
    {
	return(GetBook(BibleJson, BookNumber)->u.object.values[ChapterNumber].value);
    }
    return 0;
}

static bool IsValidVerseNumber(json_value *BibleJson, uint32_t BookNumber,
			       uint32_t ChapterNumber, uint32_t VerseNumber)
{
    if(IsValidChapterNumber(BibleJson, BookNumber, ChapterNumber))
    {
	return(VerseNumber + 1 >= 1 &&
	       VerseNumber + 1 <= GetChapter(BibleJson, BookNumber,
					     ChapterNumber)->u.object.length);
    }
    return false;
}

static json_value * GetVerse(json_value * BibleJson, uint32_t BookNumber,
			     uint32_t ChapterNumber, uint32_t VerseNumber)
{
    if(IsValidVerseNumber(BibleJson, BookNumber, ChapterNumber, VerseNumber - 1))
    {
	return(GetChapter(BibleJson, BookNumber,
			  ChapterNumber)->u.object.values[VerseNumber - 1].value);
    }
    return 0;
}

static void RemoveNewLine(char * A)
{
    while(*A && *A != '\n')
    {
	A++;
    }
    *A = 0;
}

int main(int argc, char * argv[])
{
    uint32_t Size = 0;
    char * BibleText =
	ReadEntireFileAndNullTerminate("..\\data\\EntireBible-CPDV.json", &Size);


    json_value * BibleJson = json_parse(BibleText, Size);
    printf("Bible Json Length: %d\n", BibleJson->u.object.values[1].value->
	   u.object.values[0].value->u.object.length);

    //PrintEntireBible(BibleJson);
    SearchForPhrase(BibleJson, "the meek");
    SearchForPhrase(BibleJson, "dragon");
    SearchForPhrase(BibleJson, "behemoth");

    printf("Book is valid? %d\n", IsValidBookNumber(BibleJson, 1));
    printf("Chapter is valid? %d\n", IsValidChapterNumber(BibleJson, 1, 1));
    printf("Verse is valid? %d\n", IsValidVerseNumber(BibleJson, 1, 1, 32));

#if 1
    // NOTE: The following line prints Genesis 1:1
    printf("Bible Json Length: %s\n", BibleJson->u.object.values[1].value->
	   u.object.values[0].value->u.object.values[0].value->u.string.ptr);

    json_value * Genesis = GetChild(BibleJson);
    Genesis = Genesis->u.object.values[2].value;
    char * Gen1 = Genesis->u.string.ptr;
    printf("%s\n", Gen1);

    printf("John 14:6 - %s\n", GetPassage(BibleJson, 49, 14, 6));
    printf("location of bull: %d\n", RabinKarp("Jeremiah was a bull frog", "bull"));
    printf("Location of %s in the Bible: %d\n", "dragon", RabinKarp(BibleText, "dragon"));

    printf("Strict Search: %s is book: %d\n", "Acts", BookNameToInt("Acts"));
    printf("Fuzzy Find: %s is book: %d\n", "Acts", FuzzyFindBookName("Acts"));

    char * Test = "Test";
    char * Test2 = Test + 4;
    printf("Difference of 2 pointers: %d\n", Test - Test2);
    printf("String to int test: %d\n", atoi("1024"));

    while(1)
    {
	char ResponseBuffer[32];
	printf("Enter Book, Chapter, and Verse: ");
	fgets(ResponseBuffer, 32, stdin);
	RemoveNewLine(ResponseBuffer);
	//printf("%s\n", ResponseBuffer);

	// NOTE: Initialize all references to -1 to indicate they were not found
	int Book = -1, Chapter = -1, Verse = -1, EndVerse = -1;
	char * BookPtr, * ChapterPtr, * VersePtr, *EndVersePtr = 0;
	char * Dial = ResponseBuffer;
    
	// TODO: Clean up the input parsing code. Make sure input
	// is aproppriate for parsing
	BookPtr = ResponseBuffer;
	while((*Dial) != ' ' && *Dial != 0)
	{
	    Dial++;
	}
	Dial++;
	ChapterPtr = Dial;

	while((*Dial) != ':' && *Dial != 0)
	{
	    Dial++;
	}
	if(*Dial)
	{
	    Dial++;
	    VersePtr = Dial;
	}

	char * SplitBook;
	Split(BookPtr, &SplitBook);
	printf("Just the Book name: %s\n", SplitBook);
	Book = FuzzyFindBookName(SplitBook);
	if(StringContainsNumber(ResponseBuffer))
	{
	    Chapter = atoi(ChapterPtr);
	}

	if(StringContains(ResponseBuffer, ":"))
	{
	    Verse = atoi(VersePtr);
	}

	if(StringContains(ResponseBuffer, "-"))
	{
	    EndVersePtr = GetPtrToChar(ResponseBuffer, '-');
	    EndVersePtr++;
	    EndVerse = atoi(EndVersePtr);
	}

	if(Book != -1)
	{
	    Book++;
	    if(Chapter != -1)
	    {
		Chapter--;
		if(Verse != -1)
		{
		    Verse--;
		    if(EndVerse != -1)
		    {
			EndVerse--;
			if(IsValidVerseNumber(BibleJson, Book, Chapter, Verse)
			   && IsValidVerseNumber(BibleJson, Book, Chapter, EndVerse))
			{
			    char ** Passages = GetMultiplePassages(BibleJson, Book, Chapter,
								   Verse, EndVerse);

			    for(int Index = 0; Index < EndVerse - Verse + 1; Index++)
			    {
				printf("%d %s\n", Index+1, Passages[Index]);
			    }
			}
		    }
		    else
		    {
			// NOTE: If there is no Ending Verse listed,
			// just print the single verse that was entered
			if(IsValidVerseNumber(BibleJson, Book, Chapter, Verse))
			{
			    printf("%s %d:%d - %s\n", SplitBook, Chapter + 1, Verse + 1,
				   GetPassage(BibleJson, Book, Chapter, Verse));
			}
		    }
		}
		else
		{
		    // NOTE: If verse is -1, we print the whole Chapter
		    if(IsValidChapterNumber(BibleJson, Book, Chapter))
		    {
			printf("%s Chapter %d\n", SplitBook, Chapter + 1);
			json_value * ChapterJson = GetChapter(BibleJson, Book, Chapter);
			for(uint32_t Index = 0; Index < ChapterJson->u.object.length; Index++)
			{
			    printf("\t%d %s\n", Index + 1,
				   GetVerse(BibleJson, Book, Chapter, Index + 1)->u.string.ptr);
			}
		    }
		}
	    }
	    else
	    {
		// TODO: If chapter is -1, print the whole Book
		json_value * BookJson = GetBook(BibleJson, Book);
		for(uint32_t ChapterIndex = 0;
		    ChapterIndex < BookJson->u.object.length;
		    ChapterIndex++)
		{
		    printf("%s Chapter %d\n", SplitBook, Chapter + 1);
		    json_value * ChapterJson = GetChapter(BibleJson, Book, ChapterIndex);
		    for(uint32_t Index = 0; Index < ChapterJson->u.object.length; Index++)
		    {
			printf("\t%d %s\n", Index + 1,
			       GetVerse(BibleJson, Book, ChapterIndex, Index + 1)->u.string.ptr);
		    }
		}
	    }
	}
	else
	{
	    printf("Book, Chapter, or Verse was invalid. Please try again.\n");
	}
    }
#endif
    //PrintAllBooks();
    
    //process_value(Genesis, 0);

    //printf("%s", BibleText + 5375000);
    //process_value(BibleJson, 3);
    //printf("object[%d].name = %s\n", Genesis->u.object.length, Genesis->u.object.values[1].name);

    return(0);
}
