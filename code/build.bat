@echo off

REM Use /wd#### to disable certain warnings

set CommonCompilerFlags=/MTd /nologo /Gm- /Oi /EHa- /WX /W3 /FC /FAsc /Zi /wd4996
set CommonLinkerFlags= -incremental:no -opt:ref 

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build
cl %CommonCompilerFlags% ..\code\cmd_bible.cpp -Fecmd_bible.exe /link %CommonLinkerFlags% /PDB:cmd_bible.pdb
popd
