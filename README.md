# Command Line Bible

A Bible on the command line. View the text of each book, chapter, and/or verse. Search for passages containing certain phrases. Uses C language json-parser and Rabin Karp algorithm to search for passages. 

Can be compiled by adding the Visual Studio Code compiler to your PATH and running the build.bat file. 
